#!/bin/bash

# Sample-tweets.sh
# This programm counts the amount of total tweets, unique tweets and retweets from 1-3-2017 at 12:00.
# Author: Joep Franssen
# Date: 11-3-2017

# The tweet_collection gets assigned all the tweets on 1-3-2017 at 12:00. Each tweet on a seperate line.
tweet_collection=$(gzip -d < /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab text)

# Each variable counts a specific part of the 'tweet_collection': the total amount of tweets, amount of unique tweets and amount of retweets.
total_tweet_amount=$(wc -l <<< "$tweet_collection")
unique_tweet_amount=$(sort -u <<< "$tweet_collection" | wc -l)
retweet_amount=$(sort -u <<< "$tweet_collection" | grep "^RT" | wc -l)

echo "Total amount of tweets: "$total_tweet_amount""
echo "Unique tweets: "$unique_tweet_amount""
echo -e "Retweets: "$retweet_amount"\n"

# The script below, prints the first 20 tweets that aren't retweets nor duplicates. The awk removes duplicates without sorting.
grep -v "^RT" <<< "$tweet_collection" | awk '!x[$0]++' | head -n 20